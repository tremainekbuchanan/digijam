
document.addEventListener("deviceready", onDeviceReady, false);


function onDeviceReady()
{

    var BlankView = Backbone.View.extend({
        initialize:function(){
            //this.test = this.options.lot;
            this.render();
        },

        render: function () {
            //var adapter = new MemoryAdapter();
           // var data = adapter.findById(parseInt(this.test));
            var source = $('#blank-template').html();
            var template = Handlebars.compile(source);
            var html = template(this.model.toJSON());
            $('#container').empty().append(this.$el);
            this.$el.html(html);

            return this;
        }

    });

    var DetailsView = Backbone.View.extend({
//    el:'#container',
        initialize:function()
        {

            this.render();
        },

        render:function(){
            var adapter = new MemoryAdapter();
            var source = $('#lot-details-template').html();
            var template = Handlebars.compile(source);
            var html = template();
            this.$el.html(html);
            $('#container').empty().append(this.$el);
            return this;
        }
    });

    var SearchView = Backbone.View.extend({
//    el:'#container',
        initialize:function()
        {
            this.render();
        },

        render:function(){
            var source = $('#search-template').html();
            var template = Handlebars.compile(source);
            var html = template();
            this.$el.html(html);
            $('#container').empty().append(this.$el);
            return this;
        },
        events:{
           // 'click #back': 'back'
            'touchstart #back': 'back',
            'touchstart #search': 'searchAvail'
        },
        
        searchAvail:function()
        {
            var adapter = new MemoryAdapter();
            var data = adapter.searchbyAvail();
            
            alert(data[0].apt_name);
            alert(data[1].apt_name);
            //alert(data[2].apt_name);
        },
        
        back: function(event)
        {
            window.history.back();
            return false;
        }

    });

 
    var AddLotView = Backbone.View.extend({
//    el:'#container',
        initialize:function()
        {
//        this.$el.empty();
            this.render();
        },

        render:function(){
            var source = $('#addlot-template').html();
            var template = Handlebars.compile(source);
            var html = template();
            this.$el.html(html);
            $('#container').empty().append(this.$el);
            return this;
        },

        events:{
            'touchstart #btnadd': 'saveLot'
        },

        saveLot:function()
        {
            alert("in AddlotView");
        }

    });

    var LotView = Backbone.View.extend({
//    el: '#container',
//    el: '#container',
        initialize:function(){
            this.render();
        },
        render: function () {
//        $('#container').html('<img src="img/loading.Gif">');
            var adapter = new MemoryAdapter();
            var source = $('#lot-list-template').html();
            var template = Handlebars.compile(source);
            var html = template(adapter.fetchAll());
            $('#container').empty().append(this.$el);
            this.$el.html(html);
            return this;
        }




        });


    var DetailedView = Backbone.View.extend({
//    el: '#container',
//    el: '#container',
        initialize:function(){
            this.render();
        },

        events:{
//                'click #back': 'back'
            'touchstart #back': 'back'
        },

        back: function(event)
        {
          window.history.back();
            return false;
        },
        render: function () {

            var source = $('#lot-details-template').html();
            var template = Handlebars.compile(source);
            var html = template(this.model.toJSON());
            $('#container').empty().append(this.$el);
            this.$el.html(html);
            return this;
        }






    });

    var AuthView = Backbone.View.extend({
//    el: '#container',
//    el: '#container',
        initialize:function(){
            this.render();
        },
        render: function () {

            var source = $('#oauth-template').html();
            var template = Handlebars.compile(source);
            var html = template();
            $('#container').empty().append(this.$el);
            this.$el.html(html);
            return this;
        },
        events:{
//            'click #back': 'back'
            'touchstart #back': 'back'
        },

        back: function(event)
        {
            window.history.back();
            return false;
        }

    });
    
     var LoadingView = Backbone.View.extend({

        initialize:function(){
            this.render();
        },
        render: function () {

            var source = $('#loading-template').html();
            var template = Handlebars.compile(source);
            var html = template();
            $('#container').empty().append(this.$el);
            this.$el.html(html);
            return this;
        },
        events:{
//            'click #back': 'back'
            'touchstart #back': 'back'
        },

        back: function(event)
        {
            window.history.back();
            return false;
        }

    });

//    var BlankView = Backbone.View.extend({
//        initialize:function(options){
//            _.extend(this, _.pick(options, "data"));
//            this.render();
//        },
//        render: function () {
//            //var t = this.options.num;
//            //var adapter = new MemoryAdapter();
//            var data = this.options.data;
//
//            var source = $('#blank-template').html();
//            var template = Handlebars.compile(source);
//            var html = template(data);
//            $('#container').empty().append(this.$el);
//            this.$el.html(html);
//
//            return this;
//        }
//
//    });
    var LotModel = Backbone.Model.extend({

    });
   
    
    AppRouter = Backbone.Router.extend({
        routes:{
            "": "home",
            "home": "home",
            "addlot": "addlot",
//            "rentnow" :"details",
            "details/:id" :"details",
            "blank/:id": "blank",
            "search": "search",
            "auth":"showAuth",
            "loading" : "loading"
        },

        loading:function()
        {
            new LoadingView();
            
        },
        
        blank:function(id)
        {
            //alert(id);
            var adapter = new MemoryAdapter();
            var data = adapter.findById(parseInt(id));
            //alert(data.apt_name);
            var lotModel = new LotModel(data);
            new BlankView({model:lotModel});
            //new BlankView();
        },

        details:function(id)
        {
            var adapter = new MemoryAdapter();
            var data = adapter.findById(parseInt(id));
            var lotModel = new LotModel(data);
            new DetailedView({model:lotModel});

        },
        home: function(){

            new LotView();

        },

        addlot:function()
        {

            new AddLotView()
        },

        rentnow:function(lotnumber)
        {
//       alert("rent");
            new RentView();
        },

        search:function()
        {
//            alert("Coming soon");
            new SearchView();
        },
        showAuth:function()
        {
            new AuthView();
        }


    });

    var router = new AppRouter();
    Backbone.history.start();

}








