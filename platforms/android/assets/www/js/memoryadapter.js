function MemoryAdapter() {

    this.initialize = function() {

        // console.log("Init");

        return "success";
    }

    this.findById = function(id){

        var lot = null;
        var l = lots.length;

        for(var i=0;i<l;i++)
        {
            if(lots[i].id === id)
            {
                lot = lots[i];
                break;
            }
        }

        return lot;
    }


    this.searchbyAvail = function()
    {
        //var found = false;
        
        var l = lots.length;
        
        var lot = [];
        
        var k = 0;
        
        for(var i=0;i<l;i++)
        {
            if(lots[i].avail === 'Yes')
            {
                lot[k] = lots[i];
                k++;
            }
        }
        
       return lot;
        
    }

//    this.findById = function(id){
//
//        var lot = null;
//        var l = lots.length;
//
//        for(var i=0;i<l;i++)
//        {
//            if(lots[i].id === id)
//            {
//                lot = lots[i];
//                break;
//            }
//        }
//
//        return lot;
//    }

    this.fetchAll = function()
    {
        return lots;
    }

    var lots = [{

        "id": 1,
        "location": "New Kingston, St. Andrew",
        "apt_name": "Courtleigh Towers",
        "address": "33-35 Trafalgar Road",
        "description": "Parking  space available in gated complex for daily rental",
        "avail":"Yes",
        "lot_number" : 10,
        "rental_period": "hour",
        "cost": 400,
        "contact": "1-876-588-6160",
        "contact_name" : "Derrick Brown",
        "remarks":""
        
    },
        {
            "id": 2,
            "location": "New Kingston, St. Andrew",
            "apt_name": "National Housing Trust",
            "description": "Municipal Parking Lot",
            "avail":"Yes",
            "lot_number" : 18,
            "rental_period": "hour",
            "cost": 150,
            "contact": "1-876-906-1100",
            "contact_name" : "Gloria Brown",
            "remarks":"After 5:00pm on weekdays, rate reduced to $50, no hourly limit"
        },

        {
            "id": 3,
            "location": "New Kingston, St. Andrew",
            "apt_name": "KSAC Parking Lot",
            "description": "Municpal Parking Lot. Company contractual rental agreement is available",
            "avail":"Yes",
            "lot_number" : 18,
            "rental_period": "hour",
            "cost": 150,
            "contact": "1-876-943-3100",
            "contact_name" : "Madge Douglas",
            "remarks":"Unauthorized vehicles will be clamped"

        },
        {
            "id": 4,
            "location": "New Kingston, St. Andrew",
            "apt_name": "Courtleigh Corporate Center",
            "description": "Roof top parking available.",
            "avail":"Yes",
            "lot_number" : 18,
            "rental_period": "monthly",
            "cost": 5000,
            "contact": "1-876-946-9000",
            "contact_name" : "Nesa Rone",
            "remarks":"Unauthorized vehicles will be clamped"

        },

        {
            "id": 5,
            "location": "New Kingston, St. Andrew",
            "apt_name": "National Housing Trust",
            "description": "Municipal Parking Lot",
            "avail":"Yes",
            "lot_number" : 18,
            "rental_period": "hour",
            "cost": 150,
            "contact": "1-876-906-1100",
            "contact_name" : "Gloria Brown",
            "remarks":"After 5:00pm, rate reduced to $50, no hourly limit"

        },

        {
            "id": 6,
            "location": "New Kingston, St. Andrew",
            "apt_name": "Worthington Aparatments",
            "description": "Gated community parking spot",
            "avail":"Yes",
            "lot_number" : 4,
            "rental_period": "monthly",
            "cost": 4000,
            "contact": "1-876-677-9999",
            "contact_name" : "Don Jones",
            "remarks":""

        }

        ];


}

//  var url;
